<?php
/**
 * Created by PhpStorm.
 * User: fhm
 * Date: 11/07/17
 * Time: 12:03
 */
namespace Fhm\MediaBundle\Services;

use Fhm\MediaBundle\DatabaseManager\FhmObjectManager;

/**
 * Class Local
 *
 * @package Fhm\MediaBundle\Services
 */
class TagService
{
    protected $objectManager;

    /**
     * TagService constructor.
     *
     * @param FhmObjectManager $manager
     */
    public function __construct(FhmObjectManager $manager)
    {
        $this->objectManager = $manager;
    }

    /**
     * @return $this
     */
    public function generate($media)
    {
        //TODO add check receive media if it is a real media when we move to model
        $tagClassName = $this->objectManager->getCurrentModelName('FhmMediaBundle:MediaTag');
        $repTag       = $this->objectManager->getCurrentRepository('FhmMediaBundle:MediaTag');
        $type         = $media->getType();
        $extension    = $media->getExtension();
        $tagType      = $repTag->findOneBy(['name' => $type]);
        $tagExtension = $repTag->findOneBy(['name' => $extension]);
        if (!$tagType) {
            $tagType = new $tagClassName;
            $tagType->setName($type);
        }
        if (!$tagExtension) {
            $tagExtension = new $tagClassName;
            $tagExtension->setName($extension);
        }
        $media->addTag($tagType);
        $media->addTag($tagExtension);
        $this->objectManager->getManager()->persist($media);
        $this->objectManager->getManager()->flush();

        return $media;
    }

}