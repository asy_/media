<?php
namespace Fhm\MediaBundle\Form\Type;

use Fhm\MediaBundle\DatabaseManager\FhmObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class MediaType
 *
 * @package Fhm\MediaBundle\Form\Type
 */
class MediaType extends AbstractType
{
    private $manager;

    public function __construct(FhmObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'attr' => ['autofocus' => true],
                'label' => 'media.label.name',
            ])
            ->add('file', FileType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'media.label.name',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'media.label.description',
            ])
            ->add('private', CheckboxType::class, [
                'attr' => ['rows' => 20],
                'label' => 'media.label.private',
            ])
            ->add('active', CheckboxType::class, [
                'label' => 'media.label.active',
            ])
            ->add('tags', TagsInputType::class, [
                'label' => 'media.label.tags',
                'required' => false,
            ])
        ;
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'media';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'class' => $this->manager->getCurrentModelName('FhmMediaBundle:Media'),
                'choice_label' => 'name',
                'cascade_validation' => true,
                'private' => true,
            )
        );
    }
}