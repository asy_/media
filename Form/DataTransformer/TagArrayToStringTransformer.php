<?php
/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Fhm\MediaBundle\Form\DataTransformer;

use Fhm\MediaBundle\DatabaseManager\FhmObjectManager;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class TagArrayToStringTransformer
 *
 * @package Fhm\MediaBundle\Form\DataTransformer
 */
class TagArrayToStringTransformer implements DataTransformerInterface
{
    private $manager;

    public function __construct(FhmObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($array)
    {
        /* @var Tag[] $array */
        return implode(',', $array);
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($string)
    {
        if ('' === $string || null === $string) {
            return [];
        }
        $names = array_filter(array_unique(array_map('trim', explode(',', $string))));
        // Get the current tags and find the new ones that should be created.
        $tags     = $this->manager->getCurrentRepository('FhmMediaBundle:MediaTag')->findBy(['name' => $names]);
        $newNames = array_diff($names, $tags);
        $tagClassName = $this->manager->getCurrentModelName('FhmMediaBundle:MediaTag');
        foreach ($newNames as $name) {
            $tag = new $tagClassName();
            $tag->setName($name);
            $tags[] = $tag;
        }
        return $tags;
    }
}
