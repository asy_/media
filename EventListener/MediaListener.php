<?php
/**
 * Created by PhpStorm.
 * User: fhm
 * Date: 11/07/17
 * Time: 16:53
 */
namespace Fhm\MediaBundle\EventListener;

use Fhm\MediaBundle\DatabaseManager\FhmObjectManager;
use Fhm\MediaBundle\Entity\Media;
use Fhm\MediaBundle\Services\TagService;
use Fhm\MediaBundle\StorageManager\StorageProvider;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class MediaListener
 *
 * @package Fhm\MediaBundle\EventListener
 */
class MediaListener
{
    private $manager;
    private $filesystem;
    private $tagService;

    /**
     * MediaListener constructor.
     *
     * @param \Fhm\MediaBundle\DatabaseManager\FhmObjectManager $manager
     */
    public function __construct(FhmObjectManager $manager, StorageProvider $provider, TagService $tagService)
    {
        $this->manager  = $manager;
        $this->filesystem = $provider;
        $this->tagService = $tagService;
    }

    public function handler(GenericEvent $event)
    {
        //TODO to change with generic Media model
        $media = $event->getSubject();
        if ($media instanceof Media) {
            $this->tagService->generate($media);
            $this->filesystem->transferFile($this->filesystem->getMediaFile($media), $this->filesystem->getMediaFile($media));
        }
    }

    /**
     * @param null $filename
     *
     * @return string
     */
    protected function getWorkingFile($filename = null) {
        if (is_null($filename)) {
            $filename = uniqid();
        }
        return sprintf('%s/%s', $this->getRootPath(), $filename);
    }

    /**
     *
     * @return mixed
     */
    protected function getRootPath() {
        $path = $this->filesystem->getRoot();
        return preg_replace('/\/$/', '', $path);
    }
}