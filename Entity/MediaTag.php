<?php
namespace Fhm\MediaBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Fhm\MediaBundle\Entity\Repository\MediaTagRepository")
 * @ORM\Table("fhm_media_tag")
 */
class MediaTag
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="Media", inversedBy="tags")
     */
    protected $medias;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->medias  = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return MediaTag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMedias()
    {
        return $this->medias;
    }

    /**
     * @param mixed $medias
     *
     * @return MediaTag
     */
    public function setMedias($medias)
    {
        $this->medias = $medias;

        return $this;
    }

    /**
     * @param Media $media
     *
     * @return $this
     */
    public function addMedia(Media $media)
    {
        if (!$this->medias->contains($media)) {
            $this->medias->add($media);
            $media->addTag($this);
        }

        return $this;
    }
    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return $this->name;
    }
    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->name;
    }
}