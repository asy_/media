<?php
/**
 * Created by PhpStorm.
 * User: fhm
 * Date: 11/07/17
 * Time: 11:34
 */
namespace Fhm\MediaBundle\StorageManager;
interface StorageInterface
{
    /**
     * get Filesystem class
     *
     * @return mixed
     */
    public function get(array $config);

    /**
     * handle
     *
     * @param $type
     *
     * @return mixed
     */
    public function handles($type);
}