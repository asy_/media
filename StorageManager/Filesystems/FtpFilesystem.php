<?php
namespace Fhm\MediaBundle\StorageManager\Filesystems;

use Fhm\MediaBundle\StorageManager\StorageInterface;
use League\Flysystem\Adapter\Ftp;
use League\Flysystem\Filesystem as Flysystem;

/**
 * Class FtpFilesystem
 *
 * @package Fhm\MediaBundle\Services
 */
class FtpFilesystem implements StorageInterface
{
    /**
     * @param array $config
     *
     * @return Flysystem
     */
    public function get(array $config)
    {
        return new Flysystem(new Ftp($config));
    }

    /**
     * @param $type
     *
     * @return bool
     */
    public function handles($type)
    {
        return strtolower($type) == 'ftp';
    }
}