<?php
namespace Fhm\MediaBundle\StorageManager\Filesystems;

use Fhm\MediaBundle\StorageManager\StorageInterface;
use League\Flysystem\Sftp\SftpAdapter;
use League\Flysystem\Filesystem as Flysystem;

/**
 * Class SftpFilesystem
 *
 * @package Fhm\MediaBundle\Services
 */
class SftpFilesystem implements StorageInterface
{
    /**
     * @param array $config
     *
     * @return Flysystem
     */
    public function get(array $config)
    {
        return new Flysystem(new SftpAdapter($config));
    }

    public function handles($type)
    {
        return strtolower($type) == 'sftp';
    }
}