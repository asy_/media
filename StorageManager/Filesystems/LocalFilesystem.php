<?php
namespace Fhm\MediaBundle\StorageManager\Filesystems;

use Fhm\MediaBundle\StorageManager\StorageInterface;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem as Flysystem;

/**
 * Class LocalFilesystem
 *
 * @package Fhm\MediaBundle\StorageManager\Filesystems
 */
class LocalFilesystem implements StorageInterface
{
    /**
     * @param array $config
     *
     * @return Flysystem
     */
    public function get(array $config)
    {
        return new Flysystem(new Local($config['root']));
    }

    /**
     * @param $type
     *
     * @return bool
     */
    public function handles($type)
    {
        return strtolower($type) == 'local';
    }
}