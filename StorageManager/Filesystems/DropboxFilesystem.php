<?php
namespace Fhm\MediaBundle\StorageManager\Filesystems;

use Dropbox\Client;
use Fhm\MediaBundle\StorageManager\StorageInterface;
use League\Flysystem\Dropbox\DropboxAdapter;
use League\Flysystem\Filesystem as Flysystem;

/**
 * Class DropboxFilesystem
 *
 * @package Fhm\MediaBundle\Services
 */
class DropboxFilesystem implements StorageInterface
{
    /**
     * @param array $config
     *
     * @return Flysystem
     */
    public function get(array $config)
    {
        $client = new Client($config['token'], $config['app']);

        return new Flysystem(new DropboxAdapter($client, $config['root']));
    }

    /**
     * @param $type
     *
     * @return bool
     */
    public function handles($type)
    {
        return strtolower($type) == 'dropbox';
    }
}