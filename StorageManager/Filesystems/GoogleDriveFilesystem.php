<?php
namespace Fhm\MediaBundle\StorageManager\Filesystems;

use Fhm\MediaBundle\StorageManager\StorageInterface;

/**
 * Class GoogleDriveFilesystem
 *
 * @package Fhm\MediaBundle\Services
 */
class GoogleDriveFilesystem implements StorageInterface
{
    /**
     * @param array $config
     *
     * @return array
     */
    public function get(array $config)
    {
        return $config;
    }

    /**
     * @param $type
     *
     * @return bool
     */
    public function handles($type)
    {
        return true;
    }
}