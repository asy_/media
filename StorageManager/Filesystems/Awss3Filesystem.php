<?php
namespace Fhm\MediaBundle\StorageManager\Filesystems;

use Aws\S3\S3Client;
use Fhm\MediaBundle\StorageManager\StorageInterface;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use League\Flysystem\Filesystem as Flysystem;

/**
 * Class Awss3Filesystem
 *
 * @package Fhm\MediaBundle\Services
 */
class Awss3Filesystem implements StorageInterface
{
    public function get(array $config)
    {
        $client = S3Client::factory([
            'credentials' => [
                'key'    => $config['key'],
                'secret' => $config['secret'],
            ],
            'region'      => $config['region'],
            'version'     => isset($config['version']) ? $config['version'] : 'latest',
        ]);

        return new Flysystem(new AwsS3Adapter($client, $config['bucket'], $config['root']));
    }

    /**
     * @param $type
     *
     * @return bool
     */
    public function handles($type)
    {
        return strtolower($type) == 'awss3';
    }
}