<?php
namespace Fhm\MediaBundle\StorageManager\Filesystems;

use Fhm\MediaBundle\StorageManager\StorageInterface;
use League\Flysystem\Rackspace\RackspaceAdapter;
use League\Flysystem\Filesystem as Flysystem;
use OpenCloud\Rackspace;

/**
 * Class RackSpaceFilesystem
 *
 * @package Fhm\MediaBundle\Services
 */
class RackSpaceFilesystem implements StorageInterface
{
    /**
     * @param array $config
     *
     * @return Flysystem
     */
    public function get(array $config)
    {
        $client    = new Rackspace($config['endpoint'], [
            'username' => $config['username'],
            'apiKey'   => $config['key'],
        ]);
        $container = $client->objectStoreService('cloudFiles', $config['zone'])->getContainer($config['container']);

        return new Flysystem(new RackspaceAdapter($container, $config['root']));
    }

    public function handles($type)
    {
        return strtolower($type) == 'rackspace';
    }
}