<?php
/**
 * Created by PhpStorm.
 * User: fhm
 * Date: 11/07/17
 * Time: 13:24
 */
namespace Fhm\MediaBundle\StorageManager;
/**
 * Class FilesystemTypeNotSupported
 *
 * @package Fhm\MediaBundle\StorageManager
 */
class FilesystemTypeNotSupported extends \Exception {}
{

}