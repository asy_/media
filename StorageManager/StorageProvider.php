<?php
/**
 * Created by PhpStorm.
 * User: fhm
 * Date: 11/07/17
 * Time: 12:36
 */
namespace Fhm\MediaBundle\StorageManager;
/**
 * Class StorageProvider
 *
 * @package Fhm\MediaBundle\StorageManager
 */
class StorageProvider
{

    /** @var [] */
    private $config = [];
    /** @var [] */
    private $filesystems = [];

    private $defaultStorage = null;

    private $filePath = null;



    /**
     * @param $config
     */
    public function __construct($config)
    {
        if (isset($config['storage']) && isset($config['current_storage'])) {
            $this->config         = $config['storage'];
            $this->defaultStorage = $config['current_storage'];
        }
    }

    /**
     * @param StorageInterface $filesystem
     */
    public function add(StorageInterface $filesystem)
    {
        $this->filesystems[] = $filesystem;
    }

    /**
     * @param $name
     *
     * @return \League\Flysystem\Filesystem
     * @throws FilesystemTypeNotSupported
     */
    public function get($name = null)
    {
        $name = $name?:$this->defaultStorage;
        $node = $this->getConfig($name);
        foreach ($this->filesystems as $filesystem) {
            if ($filesystem->handles($node['type'])) {
                return $filesystem->get($node);
            }
        }
    }

    /**
     * @param $name
     *
     * @return mixed
     * @throws \Fhm\MediaBundle\StorageManager\FilesystemTypeNotSupported
     */
    public function getConfig($name)
    {
        if (isset($this->config[$name])) {
            return $this->config[$name];
        }
        throw new FilesystemTypeNotSupported("The requested filesystem type {$name} is not currently supported.");
    }

    /**
     * @return array
     */
    public function getAvailableProviders()
    {
        return array_keys($this->config);
    }

    /**
     * @param      $filePath
     * @param null $filesystem
     *
     * @return bool
     */
    public function deleteFile($filePath, $filesystem=null)
    {
        return $this->get($filesystem)->delete($filePath);
    }

    /**
     * @param      $destinationPath
     * @param      $sourcePath
     * @param null $filesystem
     */
    public function transferFile($destinationPath, $sourcePath, $filesystem=null)
    {
        $this->get($filesystem)->writeStream($destinationPath, $this->get('local')->readStream(basename($sourcePath)));
    }

    /**
     * @param      $filename
     * @param null $filesystem
     *
     * @return mixed
     */
    public function getFile($filename, $filesystem=null)
    {
        return $filename;
    }

    /**
     * @param $filesystem
     * @param $media
     *
     * @return null|string
     */
    public function getMediaFile($media, $filesystem=null)
    {
        $filesystem = $filesystem?:$this->defaultStorage;
        $node = $this->getConfig($filesystem);
        $this->filePath = $node['root'].'datas/media/'.$media->getId().'/'.$media->getFile();

        return $this->filePath;
    }
}
