<?php
/**
 * Created by PhpStorm.
 * User: fhm
 * Date: 07/07/17
 * Time: 16:12
 */
namespace Fhm\MediaBundle\Controller;

use Fhm\MediaBundle\Events;
use Fhm\MediaBundle\Form\Type\MediaType;
use Fhm\MediaBundle\Utils\Slugger;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/media")
 * ----------------------------------
 * Class MediaController
 *
 * @package Fhm\MediaBundle\Controller
 */
class MediaController extends Controller
{
    /**
     * Lists all media entities.
     * @Route("/", name="fhm_media_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $medias = $this->get('fhm.object.manager')->getCurrentRepository('FhmMediaBundle:Media')->findAll();

        return $this->render('@FhmMedia/Media/index.html.twig', ['medias' => $medias]);
    }

    /**
     * Creates a new Media entity.
     * @Route("/new", name="fhm_media_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Slugger $slugger, EventDispatcherInterface $eventDispatcher)
    {
        $className = $this->get('fhm.object.manager')->getCurrentModelName('FhmMediaBundle:Media');
        $media     = new $className();
        $form      = $this->createForm(MediaType::class, $media)->add('saveAndCreateNew', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $media->setAlias($slugger->slugify($media->getName() ?: $media->getFilename()));
            $em = $this->get('fhm.object.manager')->getManager();
            $em->persist($media);
            $em->flush();
            $this->addFlash('success', 'media.created_successfully');
            $event = new GenericEvent($media);
            $eventDispatcher->dispatch(Events::MEDIA_CREATED, $event);
            if ($form->get('saveAndCreateNew')->isClicked()) {
                return $this->redirectToRoute('fhm_media_new');
            }

            return $this->redirectToRoute('fhm_media_index');
        }

        return $this->render('@FhmMedia/Media/new.html.twig', [
            'media' => $media,
            'form'  => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a Media entity.
     * @Route("/{id}", requirements={"id": "\d+"}, name="fhm_media_show")
     * @Method("GET")
     */
    public function showAction($id)
    {
        $media = $this->get('fhm.object.manager')->getCurrentRepository('FhmMediaBundle:Media')->findOneBy(['id' => $id]);
        return $this->render('@FhmMedia/Media/show.html.twig', ['media' => $media]);
    }

    /**
     * Displays a form to edit an existing Media entity.
     * @Route("/{id}/edit", requirements={"id": "\d+"}, name="fhm_media_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $id, Slugger $slugger)
    {
        $media = $this->get('fhm.object.manager')->getCurrentRepository('FhmMediaBundle:Media')->findOneBy(['id' => $id]);
        $form  = $this->createForm(MediaType::class, $media);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $media->setAlias($slugger->slugify($media->getName() ?: $media->getFileName()));
            $em = $this->get('fhm.object.manager')->getManager();
            $em->flush();
            $this->addFlash('success', 'media.updated_successfully');

            return $this->redirectToRoute('fhm_media_edit', ['id' => $media->getId()]);
        }

        return $this->render('@FhmMedia/Media/edit.html.twig', [
            'media' => $media,
            'form'  => $form->createView(),
        ]);
    }

    /**
     * Deletes a Media entity.
     * @Route("/{id}/delete", name="fhm_media_delete")
     * @Method("POST")
     * @Security("is_granted('delete', post)")
     */
    public function deleteAction(Request $request, $id)
    {
        $media = $this->get('fhm.object.manager')->getCurrentRepository('FhmMediaBundle:Media')->findOneBy(['id' => $id]);
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            return $this->redirectToRoute('fhm_media_index');
        }
        $media->getTags()->clear();
        $em = $this->get('fhm.object.manager')->getManager();
        $em->remove($media);
        $em->flush();
        $this->addFlash('success', 'media.deleted_successfully');

        return $this->redirectToRoute('fhm_media_index');
    }
}