<?php

namespace Fhm\MediaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;

/**
 * Class FhmMediaBundle
 *
 * @package Fhm\MediaBundle
 */
class FhmMediaBundle extends Bundle
{
}
